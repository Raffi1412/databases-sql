BY : Raffi Pratama Putra
1. Creat Database

    CREATE�TABLE�myshop;

2. Creat Table In Database
a. Table users

	CREATE�TABLE�users(�
	id�int�AUTO_INCREMENT�PRIMARY�KEY,�
	name�varchar(255)�NOT�null,�
	email�varchar(255)�NOT�null,�
	password�varchar(255)�NOT�null�
	);

 b. Table categories

	CREATE�TABLE�categories(�
	id�int(4)�AUTO_INCREMENT�PRIMARY�KEY,�
	name�varchar(255)NOT null�
	);

 c. Table Items

	CREATE�TABLE�items(�
	id�int(4)�AUTO_INCREMENT�PRIMARY�KEY,�
	name�varchar(255)�NOT�null,�
	description�varchar(255)�NOT�null,�
	price�int�NOT�null,�
	stock�int�NOT�null,�
	category_id�int�NOT�null,�
	FOREIGN�KEY(category_id)�REFERENCES�categories(id)�
	);

3. Input Data Into Table
a. Table users

	INSERT�INTO�users(name,�email,�password)
	VALUES("Jhon Doe","jhon@doe.com","jhon123"),
("Jhon Doe","jhon@doe.com","jenita123");

b. Table categories

	INSERT�INTO�categories(name)�
	VALUES("gadget"),("cloth"),("men"),("women"),("branded");

c. Table items

	INSERT�INTO�items(name,description,price,stock,category_id)�
	VALUES("Sumsang b50","hape keren dari brand sumsang",4000000,100,1),�
    	("Uniklooh","baju keren dari brand ternama",500000,50,2),�
    	("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

4. Retrive Data From Database

a. Retrieve data except password colos

	SELECT�name,�email�FROM�users;

b. Retrieve data items
    1. Data items in the table items that have a price above 1.000.000 (one million).

	SELECT�*�FROM�items�WHERE�price�>�1000000;

    2. Data items in the items table that have a similar or similar name (like) with the keywords �uniklo�, �watch�, or �sang� (choose only one).

	"uniklo"==>SELECT�*�FROM�items�WHERE�name�LIKE�'uniklo%';
	"watch"==>SELECT�*�FROM�items�WHERE�name�LIKE�'%watch';
	"sang"==>SELECT�*�FROM�items�WHERE�name�LIKE�'%sang%';

c. Displays data items joined to category data

	SELECT�items.name,�items.description,�items.price,�items.stock,�items.category_id,�categories.name�AS�kategori�FROM�items�INNER�JOIN�categories�ON�items.category_id�=�categories.id;

5. Changing Data from Database

	UPDATE�items�SET�price=2500000�WHERE�id=1;

